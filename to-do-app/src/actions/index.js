export const CONSTANTS = {
    ADD_TODO: 'ADD_TODO',
    DELETE_TODO: 'DELETE_TODO',
    EDIT_TODO: 'EDIT_TODO',
}

export function addToDo(title, content, id) {
    return {
        type: CONSTANTS.ADD_TODO,
        payload: { title, content, id } 
    }
}

export function editToDo(title, content, id, isCompleted) {
    return {
        type: CONSTANTS.EDIT_TODO,
        payload: { title, content, id, isCompleted } 
    }
}

export function deleteToDo(id) {
    return {
        type: CONSTANTS.DELETE_TODO,
        payload: { id }
    }
}
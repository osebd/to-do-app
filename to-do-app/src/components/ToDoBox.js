import React from 'react';
import { Button, Input, Checkbox } from 'antd';
import { addToDo, editToDo, deleteToDo } from '../actions';
import { store } from './../store/index'
import { connect } from 'react-redux';
import { Alert } from 'antd';

const { uuid } = require('uuidv4');

const { TextArea } = Input

class ToDoBox extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isEditingTitle: false,
            titleError: false,
            newTitle: null,
            newContent: null
        }
    }

    toggleTitleEditing = () => {
        if (this.state.isEditingTitle) {
            if (this.state.newTitle !== this.props.title && this.state.newTitle.length < 20)
                store.dispatch(editToDo(this.state.newTitle, this.props.content, this.props.id))
            else
                this.setState({ titleError: true })
        }

        this.setState({ isEditingTitle: !this.state.isEditingTitle })
    }

    saveContent = () => {
        if (this.state.newContent !== this.props.content)
            store.dispatch(editToDo(this.props.title, this.state.newContent, this.props.id))
    }

    markAsCompleted = () => {
        store.dispatch(editToDo(this.props.title, this.props.content, this.props.id, !this.props.isCompleted))
    }

    handleTitleChange = (e) => {
        this.setState({ newTitle: e.target.value })
    }

    handleContentChange = (e) => {
        this.setState({ newContent: e.target.value })
    }

    handleDelete = () => {
        store.dispatch(deleteToDo(this.props.id))
    }

    handleKeyDown = (e, src) => {
        if ((e.key === 'Enter' || e.keyCode === 27) && src === 'title')
            this.toggleTitleEditing() // could've extended for content
    }

    render() {
        return (
            <div className='td-box'>
                <div className='title'>
                    {this.state.isEditingTitle === false ? <div onClick={() => this.toggleTitleEditing()}>{this.props.title}</div>
                        : <Input autoFocus onBlur={() => this.toggleTitleEditing()}
                            onKeyDown={(e) => this.handleKeyDown(e, 'title')}
                            placeholder={this.props.title}
                            onChange={this.handleTitleChange}
                        />}

                </div>
                <div className='content'>
                    {this.state.titleError ? <Alert
                        style={{ position: 'absolute', width: '300px', zIndex: '2' }}
                        message="Error"
                        description="The title inserted is not valid(either the same or too long)"
                        type="error"
                        showIcon
                        closable={true}
                        onClose={() => this.setState({ titleError: false })}
                    /> : null}
                    <TextArea placeholder={this.props.content}
                        autoSize={{ minRows: 3, maxRows: 8 }}
                        style={{ width: '100%' }}
                        onChange={this.handleContentChange}
                        onBlur={() => this.saveContent()}
                        onKeyDown={(e) => this.handleKeyDown(e, 'content')}
                    />
                </div>
                <div className='buttons'>
                    <span>
                        <Checkbox checked={this.props.isCompleted}
                            onChange={this.markAsCompleted}
                            style={{color: 'white', fontSize: '16px', fontWeight: 'bold'}}
                        >
                            Completed
                            </Checkbox>
                        <Button type='primary'
                            style={{ backgroundColor: 'dodgerblue', borderRadius: '10px' }}
                            onClick={() => this.handleDelete()}
                        >
                            Delete
                        </Button>
                    </span>
                </div>
            </div>
        )
    }
}

export default connect()(ToDoBox);
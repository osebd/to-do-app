import React from 'react';
import ToDoBox from './ToDoBox';
import { store } from './../store/index'
import { addToDo } from '../actions';
import { uuid } from 'uuidv4';
import { connect } from 'react-redux'
import Filters from './Filters'


class ToDoContainer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            searchText: '',
            onlyInProgress: false
        }
    }

    handleCheckbox = (value) => {
        this.setState({ onlyInProgress: value })
    }

    renderBoxes = () => {
        let data = this.props.todos
        if (this.state.onlyInProgress) 
            data = data.filter(todo => todo.isCompleted === false)

        if (this.state.searchText) 
            data = data.filter(todo => todo.title.toUpperCase().indexOf(this.state.searchText.toUpperCase()) !== -1)

        return data.map(todo => {
            return <ToDoBox title={todo.title}
                content={todo.content}
                id={todo.id} key={todo.id}
                isCompleted={todo.isCompleted} />
        })
    }

    setInputText = (value) => {
        this.setState({ searchText: value })
      }
    
    render() {
        return (
            <div>
                <Filters callBack={this.setInputText}
                        handleCheckbox={this.handleCheckbox}
                />
                <div className='td-container'>
                    <div className='placeholder' onClick={() => store.dispatch(addToDo('Insert title', 'Insert content', uuid()))}>
                        Add
                </div>
                    {this.renderBoxes()}
                </div>
            </div>
        )
    }
}

const mapStateToProps = function (state) {
    return {
        todos: state.todos
    }
}

export default connect(mapStateToProps)(ToDoContainer);

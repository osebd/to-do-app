import React from 'react';
import { connect } from 'react-redux'
import ToDoContainer from './ToDoContainer'
import img from './../media/vert.png'
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './../store/index'

class App extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="App">
        <div className='header'>
          <img src={img} />
        </div>
        <div className='content'>
          <PersistGate loading={null} persistor={persistor}>
            <ToDoContainer />
          </PersistGate>
        </div>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    todos: state.todos
  }
}

export default connect(mapStateToProps)(App);

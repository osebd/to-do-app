import React from 'react';
import { SearchOutlined } from '@ant-design/icons'
import _ from 'lodash'
import { Checkbox } from 'antd';

export default class Filters extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            searchValue: null,
            onlyCompleted: false
        }

    }

    performQuery = _.debounce((text) => {
        this.props.callBack(text)
    }, 100)

    handleChange = (event) => {
        this.performQuery(event.target.value)
    }

    handleCheckBoxChange = (event) => {
        this.setState({ onlyCompleted: !this.state.onlyCompleted }, 
            () => this.props.handleCheckbox(this.state.onlyCompleted))
    }

    render() {
        return (<div className='SearchBar'>
            <span>
                <SearchOutlined className='img' />
                <input
                    className='input'
                    placeholder='Search for todo'
                    value={this.state.searchValue || ''}
                    onChange={this.handleChange}
                    autoFocus
                /></span>
            <Checkbox
                checked={this.state.onlyCompleted}
                onChange={this.handleCheckBoxChange}
            >Hide completed to-dos</Checkbox>
        </div>
        )
    }
}
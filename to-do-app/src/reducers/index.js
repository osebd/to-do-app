import { combineReducers } from "redux";
import { CONSTANTS } from '../actions';

const initialState = []

const listReducer = (state = initialState, action) => {
    let newState
    const data = action.payload
    switch (action.type) {
        case CONSTANTS.ADD_TODO:
            newState = [{
                id: data.id,
                title: data.title,
                content: data.content,
                isCompleted: false
            },...state]
            return newState;
        case CONSTANTS.DELETE_TODO:
            newState = state.filter(todo => todo.id !== data.id)
            return newState;
        case CONSTANTS.EDIT_TODO:
            const todo = state.find(todo => todo.id === data.id)
            todo.content = data.content
            todo.title = data.title
            todo.isCompleted = data.isCompleted
            newState = [...state,]
            return newState;
        default:
            return state;
    }
}

export default combineReducers({
    todos: listReducer
});
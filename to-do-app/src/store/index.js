import { createStore, compose } from "redux";
import { persistStore, persistReducer } from 'redux-persist'
import rootReducer from "../reducers";
//import createSagaMiddleware from 'redux-saga';
//import rootSaga from "../sagas";
import storage from 'redux-persist/lib/storage'

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const persistConfig = {
    key: 'root',
    storage
}

const persistedReducer = persistReducer(persistConfig, rootReducer)
//const sagaMiddleWare = createSagaMiddleware()

//const store = createStore(rootReducer, composeEnhancer(applyMiddleware(sagaMiddleWare)));

//sagaMiddleWare.run(rootSaga)

let store = createStore(persistedReducer, composeEnhancer())
let persistor = persistStore(store)

export { persistor, store }